/* 
1. Метод forEach() перебирає всі елементи масиву, але нічого не повертає, у порівнянні з матодом map().
2. Способи:
  1) array = []
  2) array.length = 0
  3) array = new Array
3. Можна перевірити методом Array.isArray()
*/

const filterItems = (arr, item) => {
  return arr.filter(el => typeof item !== typeof el)
}

console.log(filterItems(['hello', 'world', 23, '23', null], 'string'))
